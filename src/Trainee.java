public class Trainee implements Observer{

    private String name;
    private String currentExcercise;
    public Trainee (String name){
        this.name = name;
        this.currentExcercise = "None";
    }

    public void update(){
        System.out.println("Trainee: " + name + " is currently doing: " + currentExcercise);
    }

    public void lieDown(){
        currentExcercise = "LieDown";
        System.out.println("Trainee:" + name + " lies down.");
    }

    public void standUp(){
        currentExcercise = "Stand up";
        System.out.println("Trainee: " + name + " stands up.");
    }
}
