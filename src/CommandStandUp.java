public class CommandStandUp implements Command {
    private final Trainee trainee;

    public CommandStandUp(Trainee trainee) {
        this.trainee = trainee;
    }

    @Override
    public void execute() {
        trainee.standUp();
    }
}
