public class Main {
    public static void main(String[] args) {
        Trainer trainer = new Trainer();

        Trainee trainee = new Trainee("Ivan");

        Viewer viewer1 = new Viewer("Viewer 1");
        Viewer viewer2 = new Viewer("Viewer 2");
        Viewer viewer3 = new Viewer("Viewer 3");


        trainer.addViewer(viewer1);
        trainer.addViewer(viewer2);
        trainer.addViewer(viewer3);

        Command lieDownCmd = new CommandLieDown(trainee);
        Command standUpCmd = new CommandStandUp(trainee);

        trainer.standUp();
        standUpCmd.execute();
        System.out.println("");
        trainer.lieDown();
        lieDownCmd.execute();    }
}