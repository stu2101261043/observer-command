public class CommandLieDown implements Command {

    private final Trainee trainee;

    public CommandLieDown(Trainee trainee) {
        this.trainee = trainee;
    }

    @Override
    public void execute() {
        trainee.lieDown();
    }


}
