import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

public class Trainer {
    private final List<Viewer> viewersList = new ArrayList<>();

    public void addViewer(Viewer viewer){
        viewersList.add(viewer);
    }

    public void removeViewer(Viewer viewer){
        viewersList.remove(viewer);
    }

    public void notifyViewer(){
        for(Viewer viewer : viewersList){
            viewer.update();
        }
    }

    public void standUp(){
        System.out.println("Trainer: Stand up!");
        notifyViewer();
    }

    public void lieDown(){
        System.out.println("Trainer: Lie Down!");
        notifyViewer();
    }

}
