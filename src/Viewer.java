public class Viewer implements Observer {

    private String name;

    public Viewer(String name){
        this.name = name;
    }

    public void update(){
        System.out.println(name + ": Follows the trainer's command");
    }
}
